# generating permutations and combinations

Given N: generation all combination of 1 to N

[Recursive version](./core.ts):
Generate permutations of a set of items

[Non-Recursive version](./non-recursive-version.ts) (Faster but harder):
Generate combinations of a set of items

Bonus challenge: Generate all permutations *and* combinations of a set of items

### Combination Examples

#### Example: N = 1

Combinations: [1]

#### Example: N = 2

Combinations: [1], [2], [1,2]

#### Example: N = 3
Combinations:
  [1], [2], [3],
  [1,2], [1,3], [2,3],
  [1,2,3]

### Permutation Examples

#### Example: items = [1]

Permutations:
  [1]

#### Example: items = [1,2]

Permutations:
  [1,2],
  [2,1]

#### Example: items = [1,2,3]

Permutations:
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
