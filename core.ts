export function combinations<T>(elements: T[]): T[][] {
    if (elements.length === 0) {
        return []
    }
    if (elements.length === 1) {
        let one = elements[0]
        return [[one]]
    }
    let result: T[][] = []
    for (let i = 0; i < elements.length; i++) {
        let first = elements[i]
        let reminding = elements.slice()
        reminding.splice(i, 1)
        let subCombinations = combinations(reminding)
        for (let subCombination of subCombinations) {
            for (let slotToInsert = 0; slotToInsert < subCombination.length; slotToInsert++) {
                let combination = subCombination.slice()
                combination.splice(slotToInsert, 0, first)
                result.push(combination)
            }
        }
    }
    return result
}

