import {combinations} from "./core";

it('should generate combination of 1 element', function () {
    let combinationResult = combinations([1]);
    expect(combinationResult).toContainEqual([1])
});

it('should generate combination of 2 elements', function () {
    let combinationResult = combinations([1, 2]);
    expect(combinationResult).toContainEqual([1, 2])
    expect(combinationResult).toContainEqual([2, 1])
});

it('should generate combination of 3 elements', function () {
    let combinationResult = combinations([1, 2, 3]);

    expect(combinationResult).toContainEqual([1, 2, 3])
    expect(combinationResult).toContainEqual([1, 3, 2])

    expect(combinationResult).toContainEqual([2, 1, 3])
    expect(combinationResult).toContainEqual([2, 3, 1])

    expect(combinationResult).toContainEqual([3, 1, 2])
    expect(combinationResult).toContainEqual([3, 2, 1])
});

it('should generate combination of 4 elements', function () {
    let combinationResult = combinations([1, 2, 3, 4]);

    // start from 1
    expect(combinationResult).toContainEqual([1, 2, 3, 4])
    expect(combinationResult).toContainEqual([1, 2, 4, 3])

    expect(combinationResult).toContainEqual([1, 3, 2, 4])
    expect(combinationResult).toContainEqual([1, 3, 4, 2])

    expect(combinationResult).toContainEqual([1, 4, 2, 3])
    expect(combinationResult).toContainEqual([1, 4, 3, 2])

    // start from 2
    expect(combinationResult).toContainEqual([2, 1, 3, 4])
    expect(combinationResult).toContainEqual([2, 1, 4, 3])

    expect(combinationResult).toContainEqual([2, 3, 1, 4])
    expect(combinationResult).toContainEqual([2, 3, 4, 1])

    expect(combinationResult).toContainEqual([2, 4, 1, 3])
    expect(combinationResult).toContainEqual([2, 4, 3, 1])

    // start from 3
    expect(combinationResult).toContainEqual([3, 2, 1, 4])
    expect(combinationResult).toContainEqual([3, 2, 4, 1])

    expect(combinationResult).toContainEqual([3, 1, 2, 4])
    expect(combinationResult).toContainEqual([3, 1, 4, 2])

    expect(combinationResult).toContainEqual([3, 4, 2, 1])
    expect(combinationResult).toContainEqual([3, 4, 1, 2])

    // start from 4
    expect(combinationResult).toContainEqual([4, 2, 1, 3])
    expect(combinationResult).toContainEqual([4, 2, 3, 1])

    expect(combinationResult).toContainEqual([4, 1, 2, 3])
    expect(combinationResult).toContainEqual([4, 1, 3, 2])

    expect(combinationResult).toContainEqual([4, 3, 2, 1])
    expect(combinationResult).toContainEqual([4, 3, 1, 2])
});
